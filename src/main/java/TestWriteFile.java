
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestWriteFile {
    public static void main(String[]args){
        File file = new File("ox.bin");
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
      
        Player o = new Player('o');
        Player x = new Player('x');
        o.win();
        x.lose();
        x.win();
        o.lose();
        o.draw();
        x.draw();

        try {
            file = new File("ox.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos); 
            oos.writeObject(o);
            oos.writeObject(x);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if(oos!=null){
                    fos.close();
                }
                if(fos!=null){
                    fos.close();
                }
                
            } catch (IOException ex) {
                Logger.getLogger(TestWriteFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
